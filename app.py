import os
import time
from pathlib import Path
from tkinter import *
from tkinter import filedialog

import tkinter.ttk as ttk

import pygame
from mutagen.mp3 import MP3
from mutagen import MutagenError

root = Tk()

root.title('Codemy.com MP3 Player')
root.iconbitmap('codemy.ico')
root.geometry("500x400")

# Initialize the player
pygame.mixer.init()

# Global variables

# song base directory
# global song_dir
song_dir = ""

# Create global pause variable
# global paused
paused = False

# song length
song_length = 0

# To stop playing current song
stopped = False

list_songs = {}


def play_time():
    """ Grab song length Time Info """

    global song_dir
    global song_length
    global stopped

    # Check to double timing (continue looping when changing song)
    if stopped:
        return

    # Get the current song tuple number
    # current_song = song_box.curselection()
    # Grab song title from playlist
    song = song_box.get(ACTIVE)

    # get song dir
    global list_songs
    song_dir = list_songs[song]

    #  Grab song title to playlist
    song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

    # If song does not exists pass
    if os.path.exists(song) == False:
        return

    # Load song length with Mutagen
    try:
        song_mut = MP3(song)
    except MutagenError as ex:
        # print(ex)
        pass

    # Get song length
    song_length = song_mut.info.length

    # convert to time format
    time_format = '%M:%S' if (song_length < 3600) else '%H:%M:%S'

    converted_song_length = time.strftime(
        time_format, time.gmtime(song_length))

    # Grab current Song elapsed time
    current_time = pygame.mixer.music.get_pos() / 1000

    # Increase current time by 1 second
    current_time += 1

    if int(my_slider.get()) == int(song_length):
        status_bar.config(
            text=f'Time Elapsed: {converted_song_length} of {converted_song_length} ')
    elif paused:
        pass
    elif int(my_slider.get()) == int(current_time):
        # Slider hasn't been moved
        # Update slider to position
        slider_position = int(song_length)
        my_slider.config(to=slider_position, value=int(current_time))
    else:
        # Slider has been moved
        # Update slider to position
        slider_position = int(song_length)
        my_slider.config(to=slider_position, value=int(my_slider.get()))

        # convert to time format
        converted_current_time = time.strftime(
            time_format, time.gmtime(int(my_slider.get())))

        # Output time to status bar
        status_bar.config(
            text=f'Time Elapsed: {converted_current_time} of {converted_song_length} ')

        # Move this this along by one second
        next_time = int(my_slider.get()) + 1
        my_slider.config(value=next_time)

    #     #  throw up temp label to get data
    # slider_label.config(text=f'Slider: {int(my_slider.get())} and Song Pos: {int(current_time)}')

    # # convert to time format
    # converted_current_time = time.strftime(
    #     time_format, time.gmtime(current_time))

    # status_bar.config(
    #     text=f'Time Elapsed: {converted_current_time} of {converted_song_length} ')

    # Update slider position value to current song position
    # my_slider.config(value=current_time)

    # update time
    status_bar.after(1000, play_time)


def add_song():
    """ Add Song """
    global song_dir

    song = filedialog.askopenfilename(
        initialdir='audio/', title="Choose A Song", filetypes=(("mp3 Files", "*.mp3"), ))

    song_dir = Path(song).parent
    song = Path(song).stem

    # Add song to listbox
    song_box.insert(END, song)
    # print(song_dir)

    # add to song list
    global list_songs
    list_songs[song] = song_dir


def add_many_songs():
    """ Add many song to playlist """
    global song_dir

    songs = filedialog.askopenfilenames(
        initialdir='audio/', title="Choose A Song", filetypes=(("mp3 Files", "*.mp3"), ))

    song_dir = Path(songs[0]).parent

    # Loop through song list and replace directory info and mp3
    for song in songs:
        song = Path(song).stem
        # Insert song into songlist
        song_box.insert(END, song)

        # add to song list
        global list_songs
        list_songs[song] = song_dir


def delete_song():
    """ Delete A Song from the playlist """

    stop()

    # remove from song list
    global list_songs
    current_song = song_box.get(ANCHOR)
    if current_song in list_songs:
        del list_songs[song_box.get(ANCHOR)]

        song_box.delete(ANCHOR)
        # Stop music if it's playing
        pygame.mixer.music.stop()


def delete_all_songs():
    """ Delete All Songs from the playlist """

    stop()

    # print(len(song_box.get(0, END)))
    song_box.delete(0, END)
    # Stop music if it's playing
    pygame.mixer.music.stop()

    # remove from song list
    global list_songs
    list_songs.clear()


def play():
    """ Play selected song """
    global song_dir

    # Set Stopped Variable to False so song can play
    global stopped
    global list_songs

    # If no songs do nothing
    if len(list_songs) == 0:
        return
    stopped = False

    song = song_box.get(ACTIVE)

    if song == '':
        return

    # get song dir
    song_dir = list_songs[song]

    song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

    pygame.mixer.music.load(song)
    pygame.mixer.music.play(loops=0)

    # Call the play_time function to get song length
    play_time()

    # # Update slider to position
    # slider_position = int(song_length)
    # my_slider.config(to=slider_position, value=0)


def stop():
    """ Stop playing current song """

    # Reset Slider and Status Bar
    status_bar.config(text='')
    my_slider.config(value=0)

    # Stop Song  from Playing
    pygame.mixer.music.stop()
    song_box.select_clear(ACTIVE)

    # Clear the status bar
    status_bar.config(text='')

    # Set Stop Variable to True
    global stopped
    stopped = True


def next_song():
    """ Play the Next Song in the playlist """
    global song_dir
    global list_songs

    # If no songs do nothing
    if len(list_songs) == 0:
        return

    # Reset Slider and Status Bar
    status_bar.config(text='')
    my_slider.config(value=0)

    # Get the current song tuple number
    next_one = song_box.curselection()

    # print('next', next_one, song_box.size())
    # No selection
    if len(next_one) == 0:
        return
    if next_one[0] == song_box.size() - 1:
        next_one = 0
    else:
        # Add one to the current song tuple number
        next_one = next_one[0] + 1

    song = song_box.get(next_one)

    # get song dir
    song_dir = list_songs[song]

    #  Grab song title to playlist
    song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

    pygame.mixer.music.load(song)
    pygame.mixer.music.play(loops=0)

    # Clear active bar in playlist
    song_box.selection_clear(0, END)

    # Activate nex song in list
    song_box.activate(next_one)

    song_box.selection_set(next_one, last=None)


def previous_song():
    """ Play previous song """
    global song_dir
    global list_songs

    # If no songs do nothing
    if len(list_songs) == 0:
        return

    # Reset Slider and Status Bar
    status_bar.config(text='')
    my_slider.config(value=0)

    # Get the current song tuple number
    prev_song = song_box.curselection()

    # print('next', prev_song, song_box.size() - 1)
    # No selection
    if len(prev_song) == 0:
        return

    if prev_song[0] == 0:
        prev_song = song_box.size() - 1
    else:
        # Add one to the current song tuple number
        prev_song = prev_song[0] - 1

    song = song_box.get(prev_song)

    # get song dir
    song_dir = list_songs[song]

    #  Grab song title to playlist
    song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

    pygame.mixer.music.load(song)
    pygame.mixer.music.play(loops=0)

    # Clear active bar in playlist
    song_box.selection_clear(0, END)

    # Activate nex song in list
    song_box.activate(prev_song)

    song_box.selection_set(prev_song, last=None)


def pause(is_paused):
    """ Pause and Unpause the current Song """
    global paused
    paused = is_paused

    if is_paused:
        # Unpause
        pygame.mixer.music.unpause()
        paused = False
    else:
        # Pause
        pygame.mixer.music.pause()
        paused = True


def slide(x):
    '''' Create slider function '''
    # slider_label.config(text=f'{int(my_slider.get())} of {int(song_length)}')
    global song_dir

    song = song_box.get(ACTIVE)

    # get song dir
    global list_songs
    song_dir = list_songs[song]

    song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

    pygame.mixer.music.load(song)
    pygame.mixer.music.play(loops=0, start=int(my_slider.get()))


# Create Playlist Box
song_box = Listbox(root, bg="black", fg="beige", width=60,
                   selectbackground="lightgray", selectforeground="black")
song_box.pack(pady=20)

# Define Player Control Button Images
back_btn_img = PhotoImage(file='images/back50.png')
forward_btn_img = PhotoImage(file='images/forward50.png')
play_btn_img = PhotoImage(file='images/play50.png')
pause_btn_img = PhotoImage(file='images/pause50.png')
stop_btn_img = PhotoImage(file='images/stop50.png')

# Create Player Control Frame
controls_frame = Frame(root)
controls_frame.pack()

# Create Player Control Buttons
back_button = Button(controls_frame, image=back_btn_img,
                     borderwidth=0, command=previous_song)
forward_button = Button(
    controls_frame, image=forward_btn_img, borderwidth=0, command=next_song)
play_button = Button(controls_frame, image=play_btn_img,
                     borderwidth=0, command=play)
pause_button = Button(controls_frame, image=pause_btn_img,
                      borderwidth=0, command=lambda: pause(paused))
stop_button = Button(controls_frame, image=stop_btn_img,
                     borderwidth=0, command=stop)

back_button.grid(row=0, column=0, padx=10)
forward_button.grid(row=0, column=1, padx=10)
play_button.grid(row=0, column=2, padx=10)
pause_button.grid(row=0, column=3, padx=10)
stop_button.grid(row=0, column=4, padx=10)

# Create Menu
my_menu = Menu(root)
root.config(menu=my_menu)

# Add Add Song Menu
add_song_menu = Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="Add Songs", menu=add_song_menu)
# Add a song
add_song_menu.add_command(label="Add One Song To Playlist", command=add_song)
# Add many songs
add_song_menu.add_command(
    label="Add Many Songs To Playlist", command=add_many_songs)

# Create Delete Song Menu
remove_song_menu = Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="Remove Songs", menu=remove_song_menu)
# Remove a song
remove_song_menu.add_command(
    label="Remove a Song From Playlist", command=delete_song)
# Remove many songs
remove_song_menu.add_command(
    label="Remove Many Songs From Playlist", command=delete_all_songs)


# Status Bar
status_bar = Label(root, bd=1, relief=GROOVE, anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=2)

# Create Music Position Slider
my_slider = ttk.Scale(root, from_=0, to=100, orient=HORIZONTAL,
                      command=lambda x: slide(x), value=0, length=360)
my_slider.pack(pady=20)

# slider_label = Label(root, text="0")
# slider_label.pack(pady=10)

root.mainloop()
