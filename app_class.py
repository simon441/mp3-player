import os
import time
from pathlib import Path
from tkinter import *
from tkinter import filedialog

import tkinter.ttk as ttk

import pygame
from mutagen.mp3 import MP3
from mutagen import MutagenError


class Mp3Player(object):

    def __init__(self, master, base_path):

        self.master = master
        self.base_path = base_path

        # Initialize the player
        pygame.mixer.init()

        # Global variables

        # song base directory
        self.song_dir = ""

        # Create pause variable
        self.paused = False

        # song length
        self.song_length = 0

        # To stop playing current song
        self.stopped = False

        #  Song list
        self.list_songs = {}

        # Create Playlist Box
        self.song_box = Listbox(self.master, bg="black", fg="beige", width=60,
                                selectbackground="lightgray", selectforeground="black")
        self.song_box.pack(pady=20)

        # Define Player Control Button Imagselfes
        self.back_btn_img = PhotoImage(
            file=self._make_path('images/back50.png'))
        self.forward_btn_img = PhotoImage(
            file=self._make_path(os.path.join('images', 'forward50.png')))
        self.play_btn_img = PhotoImage(
            file=self._make_path(os.path.join('images', 'play50.png')))
        self.pause_btn_img = PhotoImage(
            file=self._make_path(['images', 'pause50.png']))
        self.stop_btn_img = PhotoImage(
            file=self._make_path(('images', 'stop50.png')))

        # Create Player Control Frame
        controls_frame = Frame(self.master)
        controls_frame.pack()

        # Create Player Control Buttons
        back_button = Button(controls_frame, image=self.back_btn_img,
                             borderwidth=0, command=self.previous_song)
        forward_button = Button(
            controls_frame, image=self.forward_btn_img, borderwidth=0, command=self.next_song)
        play_button = Button(controls_frame, image=self.play_btn_img,
                             borderwidth=0, command=self.play)
        pause_button = Button(controls_frame, image=self.pause_btn_img,
                              borderwidth=0, command=lambda: self.pause(self.paused))
        stop_button = Button(controls_frame, image=self.stop_btn_img,
                             borderwidth=0, command=self.stop)

        back_button.grid(row=0, column=0, padx=10)
        forward_button.grid(row=0, column=1, padx=10)
        play_button.grid(row=0, column=2, padx=10)
        pause_button.grid(row=0, column=3, padx=10)
        stop_button.grid(row=0, column=4, padx=10)

        self.__create_menu()

        # Status Bar
        self.status_bar = Label(self.master, bd=1, relief=GROOVE, anchor=E)
        self.status_bar.pack(fill=X, side=BOTTOM, ipady=2)

        # Create Music Position Slider
        self.my_slider = ttk.Scale(self.master, from_=0, to=100, orient=HORIZONTAL,
                                   command=lambda x: self.slide(x), value=0, length=360)
        self.my_slider.pack(pady=20)

    def __create_menu(self):
        """ Create Menu """

        #  Master menu
        my_menu = Menu(self.master)
        self.master.config(menu=my_menu)

        # Create Add Song Menu
        add_song_menu = Menu(my_menu, tearoff=False)
        my_menu.add_cascade(label="Add Songs", menu=add_song_menu)
        # Add a song
        add_song_menu.add_command(
            label="Add One Song To Playlist", command=self.add_song)
        # Add many songs
        add_song_menu.add_command(
            label="Add Many Songs To Playlist", command=self.add_many_songs)

        # Create Delete Song Menu
        remove_song_menu = Menu(my_menu, tearoff=False)
        my_menu.add_cascade(label="Remove Songs", menu=remove_song_menu)

        # Remove a song
        remove_song_menu.add_command(
            label="Remove a Song From Playlist", command=self.delete_song)
        # Remove many songs
        remove_song_menu.add_command(
            label="Remove Many Songs From Playlist", command=self.delete_all_songs)

    def play_time(self):
        """ Grab song length Time Info """

        # Check to double timing (continue looping when changing song)
        if self.stopped:
            return

        # Grab song title from playlist
        song = self.song_box.get(ACTIVE)

        # get song dir
        self.song_dir = self.list_songs[song]

        # Grab song title to playlist
        song = os.fspath(Path.joinpath(self.song_dir, f'{song}.mp3'))

        # If song does not exists pass
        if os.path.exists(song) == False:
            return

        # Load song length with Mutagen
        try:
            song_mut = MP3(song)
        except MutagenError as ex:
            # print(ex)
            pass

        # Get song length
        self.song_length = song_mut.info.length

        # convert to time format
        self.time_format = '%M:%S' if (self.song_length < 3600) else '%H:%M:%S'

        self.converted_song_length = time.strftime(
            self.time_format, time.gmtime(self.song_length))

        # Grab current Song elapsed time
        current_time = pygame.mixer.music.get_pos() / 1000

        # Increase current time by 1 second
        current_time += 1

        if int(self.my_slider.get()) == int(self.song_length):
            self.status_bar.config(
                text=f'Time Elapsed: {self.converted_song_length} of {self.converted_song_length} ')
        elif self.paused:
            pass
        elif int(self.my_slider.get()) == int(current_time):
            # Slider hasn't been moved
            # Update slider to position
            self.slider_position = int(self.song_length)
            self.my_slider.config(to=self.slider_position,
                                  value=int(current_time))
        else:
            # Slider has been moved
            # Update slider to position
            self.slider_position = int(self.song_length)
            self.my_slider.config(to=self.slider_position,
                                  value=int(self.my_slider.get()))

            # convert to time format
            converted_current_time = time.strftime(
                self.time_format, time.gmtime(int(self.my_slider.get())))

            # Output time to status bar
            self.status_bar.config(
                text=f'Time Elapsed: {converted_current_time} of {self.converted_song_length} ')

            # Move this this along by one second
            next_time = int(self.my_slider.get()) + 1
            self.my_slider.config(value=next_time)

        #     #  throw up temp label to get data
        # slider_label.config(text=f'Slider: {int(self.my_slider.get())} and Song Pos: {int(current_time)}')

        # # convert to time format
        # converted_current_time = time.strftime(
        #     self., time.gmtime(current_time))

        # self.status_bar.config(
        #     text=f'Time Elapsed: {converted_current_time} of {self.converted_song_length} ')

        # Update slider position value to current song position
        # self.my_slider.config(value=current_time)

        # update time
        self.status_bar.after(1000, self.play_time)

    def add_song(self):
        """ Add Song """

        song = filedialog.askopenfilename(
            initialdir='audio' + os.path.sep, title="Choose A Song", filetypes=(("mp3 Files", "*.mp3"), ))

        self.song_dir = Path(song).parent
        song = Path(song).stem

        # Add song to listbox
        self.song_box.insert(END, song)
        # print(self.song_dir)

        # add to song list
        # global self.list_songs
        self.list_songs[song] = self.song_dir

    def add_many_songs(self):
        """ Add many song to playlist """

        songs = filedialog.askopenfilenames(
            initialdir='audio' + os.path.sep, title="Choose A Song", filetypes=(("mp3 Files", "*.mp3"), ))

        self.song_dir = Path(songs[0]).parent

        # Loop through song list and replace directory info and mp3
        for song in songs:
            song = Path(song).stem
            # Insert song into songlist
            self.song_box.insert(END, song)

            # add to song list
            self.list_songs[song] = self.song_dir

    def delete_song(self):
        """ Delete A Song from the playlist """

        self.stop()

        # remove from song list if selected
        current_song = self.song_box.get(ANCHOR)
        if current_song in self.list_songs:
            del self.list_songs[current_song]

        self.song_box.delete(ANCHOR)
        # Stop music if it's playing
        pygame.mixer.music.stop()

    def delete_all_songs(self):
        """ Delete All Songs from the playlist """

        self.stop()

        # print(len(self.song_box.get(0, END)))
        self.song_box.delete(0, END)
        # Stop music if it's playing
        pygame.mixer.music.stop()

        # remove from song list
        self.list_songs.clear()

    def play(self):
        """ Play selected song """

        # Set Stopped Variable to False so song can play

        if self.song_box.size() == 0:
            return

        self.stopped = False

        song = self.song_box.get(ACTIVE)

        # get song dir
        if song not in self.list_songs:
            return
        self.song_dir = self.list_songs[song]

        song = os.fspath(Path.joinpath(self.song_dir, f'{song}.mp3'))

        pygame.mixer.music.load(song)
        pygame.mixer.music.play(loops=0)

        # Call the play_time function to get song length
        self.play_time()

        # # Update slider to position
        # self.slider_position = int(song_length)
        # self.my_slider.config(to=self.slider_position, value=0)

    def stop(self):
        """ Stop playing current song """
        if self.song_box.size() == 0:
            return

        # Reset Slider and Status Bar
        self.status_bar.config(text='')
        self.my_slider.config(value=0)

        # Stop Song from Playing
        pygame.mixer.music.stop()
        self.song_box.select_clear(ACTIVE)

        # Clear the status bar
        self.status_bar.config(text='')

        # Set Stop Variable to True
        self.stopped = True

    def next_song(self):
        """ Play the Next Song in the playlist """

        # If no songs do nothing
        if len(self.list_songs) == 0:
            return

        # Get the current song tuple number
        next_one = self.song_box.curselection()

        # print('next', next_one, self.song_box.size())
        # Selected a value
        if len(next_one) > 0:

            # Reset Slider and Status Bar
            self.status_bar.config(text='')
            self.my_slider.config(value=0)

            if next_one[0] == self.song_box.size() - 1:
                next_one = 0
            else:
                # Add one to the current song tuple number
                next_one = next_one[0] + 1

            song = self.song_box.get(next_one)

            # get song dir
            if song not in self.list_songs:
                return
            self.song_dir = self.list_songs[song]

            #  Grab song title to playlist
            song = os.fspath(Path.joinpath(self.song_dir, f'{song}.mp3'))

            pygame.mixer.music.load(song)
            pygame.mixer.music.play(loops=0)

            # Clear active bar in playlist
            self.song_box.selection_clear(0, END)

            # Activate next song in list
            self.song_box.activate(next_one)

            self.song_box.selection_set(next_one, last=None)

    def previous_song(self):
        """ Play previous song """

        if self.song_box.size() == 0:
            return

        # If no songs do nothing
        if len(self.list_songs) == 0:
            return

        # Get the current song tuple number
        prev_song = self.song_box.curselection()

        # print('next', prev_song, self.song_box.size()-1)
        # Selected a value
        if len(prev_song) > 0:

            # Reset Slider and Status Bar
            self.status_bar.config(text='')
            self.my_slider.config(value=0)

            if prev_song[0] == 0:
                prev_song = self.song_box.size() - 1
            else:
                # Add one to the current song tuple number
                prev_song = prev_song[0] - 1

            song = self.song_box.get(prev_song)

            # get song dir
            song_dir = self.list_songs[song]

            # Grab song title to playlist
            song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

            pygame.mixer.music.load(song)
            pygame.mixer.music.play(loops=0)

            # Clear active bar in playlist
            self.song_box.selection_clear(0, END)

            # Activate nex song in list
            self.song_box.activate(prev_song)

            self.song_box.selection_set(prev_song, last=None)

    def pause(self, is_paused):
        """ Pause and Unpause the current Song """
        if self.song_box.size() == 0:
            return

        self.paused = is_paused

        if is_paused:
            # Unpause
            pygame.mixer.music.unpause()
            self.paused = False
        else:
            # Pause
            pygame.mixer.music.pause()
            self.paused = True

    def slide(self, x):
        '''' Create slider function '''
        # slider_label.config(text=f'{int(self.my_slider.get())} of {int(song_length)}')
        if self.song_box.size() == 0:
            return

        song = self.song_box.get(ACTIVE)

        # get song dir
        song_dir = self.list_songs[song]

        song = os.fspath(Path.joinpath(song_dir, f'{song}.mp3'))

        pygame.mixer.music.load(song)
        pygame.mixer.music.play(loops=0, start=int(self.my_slider.get()))

    def _make_path(self, components):
        """ Create a string path with os aware path eparators (eg: / or \)
        components: list, tuple, str
         """
        if type(components) is str:
            c = components
        else:
            c = os.path.sep.join(components)
        return self.base_path + os.path.sep + c


root = Tk()
root.title('Codemy.com MP3 Player')
root.iconbitmap('codemy.ico')
root.geometry("500x400")
# print(os.path.abspath(os.path.curdir), os.path.curdir)
# print(os.path.join(os.path.abspath(os.path.curdir), 'images/image.png'))
app = Mp3Player(root, os.path.abspath(os.path.curdir))
root.mainloop()
