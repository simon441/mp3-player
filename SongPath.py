class SongPath(object):
    """ Manage song path """

    song_list = {}

    def __init__(self, path):
        """ path is the absolute path to the song """
        super().__init__()
        self._song_path = path
        song = ""
        self.song_list.append(song, path)

    def get_path(self):
        """ get the path """
        return self._song_path

    def set_path(self, path):
        """ Set the path """
        self._song_path = path

    def resolve(self, song_name, ext = ''):
        """ Get the path to the song 
            song_name: name of the file
            ext: file extension if needed
        """
        pass

    def get_song(self, song_name):
        """ get the song path """
        return self.song_list[song_name]